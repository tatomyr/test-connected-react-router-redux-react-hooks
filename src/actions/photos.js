export const types = {
  GET_LIST: {
    STARTED: "GET_LIST.STARTED",
    SUCCESS: "GET_LIST.SUCCESS",
    FAILURE: "GET_LIST.FAILURE"
  },
  INCREASE_COUNT: "INCREASE_COUNT"
}

export const getList = () => ({ type: types.GET_LIST.STARTED })

export const inc = () => ({ type: types.INCREASE_COUNT })
