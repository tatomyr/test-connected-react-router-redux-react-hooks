import React from "react"
import { Switch, Route } from "react-router-dom"
import { Header, PhotosList, PhotoView } from "components"

export const App = () => (
  <>
    <Header />
    <main>
      <Switch>
        <Route
          exact
          path="/"
          component={() => (
            <>
              <p>
                <span>Test SPA with</span> <code>connected-react-router</code>
              </p>
              <p>
                Photos shamelessly taken from{" "}
                <a href="https://picsum.photos/">https://picsum.photos/</a>
              </p>
            </>
          )}
        />
        <Route exact path="/photos" component={PhotosList} />
        <Route exact path="/photo/:id" component={PhotoView} />
      </Switch>
    </main>
  </>
)
