import React from "react"
import { NavLink } from "react-router-dom"

export const Header = () => (
  <header>
    <NavLink exact to="/">
      Home
    </NavLink>
    {" | "}
    <NavLink exact to="/photos">
      Photos
    </NavLink>
  </header>
)

export default Header
