import React from "react"
import { Link } from "react-router-dom"
import VisibilitySensor from "react-visibility-sensor"

export const PhotoItem = ({ id, author, author_url, filename }) => (
  <li key={id}>
    <Link to={`/photo/${id}`}>
      <img src={`https://picsum.photos/100?image=${id}`} alt={filename} />
    </Link>
    <div>
      <a href={author_url}>{author} →</a>
    </div>
  </li>
)

export const LoadMore = ({ handleFire }) => (
  <li>
    <VisibilitySensor onChange={isVisible => isVisible && handleFire()}>
      <button onClick={handleFire}>
        <div>Load More</div>
      </button>
    </VisibilitySensor>
  </li>
)
