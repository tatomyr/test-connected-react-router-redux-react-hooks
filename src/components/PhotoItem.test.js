import React from "react"
import { shallow } from "enzyme"
import { PhotoItem } from "./PhotoItem"

describe("<PhotoItem />", () => {
  it("should match a snapshot", () => {
    const wrapper = shallow(
      <PhotoItem
        id="0"
        author="Test Author"
        author_url="test.url"
        filename="test"
      />
    )
    expect(wrapper).toMatchSnapshot()
  })
})
