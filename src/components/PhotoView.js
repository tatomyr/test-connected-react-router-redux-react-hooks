import React, { useEffect } from "react"
import { connect } from "react-redux"
import { getList } from "actions/photos"
import { selectImageInfo } from "selectors/photos"
import { imagePath } from "services/photos"

export const PhotoView = ({ id, info, ...actions }) => {
  useEffect(() => {
    if (!info.hasOwnProperty("author")) actions.getList()
  }, [])
  return (
    <>
      <div className="view">
        <img src={imagePath(id, info)} alt={id} />
      </div>
      <div>{info.author}</div>
    </>
  )
}

const mapStateToProps = (
  state,
  {
    match: {
      params: { id }
    }
  }
) => ({ id, info: selectImageInfo(id)(state) })

const mapDispatchToProps = {
  getList
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhotoView)
