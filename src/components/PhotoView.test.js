import React from "react"
import { mount } from "enzyme"
import { PhotoView } from "./PhotoView"

// FIXME: this test refuses to work :|

describe("<PhotoView />", () => {
  let props
  beforeEach(() => {
    props = { id: 0, info: {}, getList: jest.fn() }
  })
  it("should match a snapshot when info is not provided", async () => {
    const wrapper = mount(<PhotoView {...props} />)
    console.log("wrapper-->", wrapper)
    expect(props.getList).toHaveBeenCalled()
    expect(wrapper).toMatchSnaphot()
  })
})
