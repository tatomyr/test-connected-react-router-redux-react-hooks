import React, { useEffect } from "react"
import { connect } from "react-redux"
import { getList, inc } from "actions/photos"
import { selectVisibleList } from "selectors/photos"
import { PhotoItem, LoadMore } from "./PhotoItem"

export const PhotosList = props => {
  useEffect(() => {
    if (!props.list.length) props.getList()
  }, [])
  return (
    <ul>
      {[
        ...props.list.map(PhotoItem),
        <LoadMore key="load-more" handleFire={props.inc} />
      ]}
    </ul>
  )
}

const mapStateToProps = state => ({ list: selectVisibleList(state) })

const mapDispatchToProps = { getList, inc }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhotosList)
