import { createBrowserHistory } from "history"
import { applyMiddleware, createStore } from "redux"
import { routerMiddleware } from "connected-react-router"
import { composeWithDevTools } from "redux-devtools-extension"
import createSagaMiddleware from "redux-saga"
import { createRootReducer } from "reducers"
import { rootSaga } from "sagas"

export const history = createBrowserHistory()

const sagaMiddleware = createSagaMiddleware()

export const configureStore = () => {
  const store = createStore(
    createRootReducer(history), // root reducer with router state
    composeWithDevTools(
      applyMiddleware(
        routerMiddleware(history), // for dispatching history actions
        sagaMiddleware
      )
    )
  )

  sagaMiddleware.run(rootSaga)

  return store
}
