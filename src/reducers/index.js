import { combineReducers } from "redux"
import { connectRouter } from "connected-react-router"
import { photos } from "./photos"

export const createRootReducer = history =>
  combineReducers({
    router: connectRouter(history),
    photos
  })
