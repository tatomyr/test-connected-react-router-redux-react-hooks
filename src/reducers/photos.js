import { types } from "actions/photos"

export const initialState = {
  list: [],
  isLoading: false,
  count: 11
}

export const photos = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.GET_LIST.STARTED:
      return { ...state, isLoading: true }
    case types.GET_LIST.SUCCESS:
      return { ...state, list: action.list, isLoading: false }
    case types.GET_LIST.FAILURE:
      return { ...state, isLoading: false }
    case types.INCREASE_COUNT:
      return { ...state, count: state.count + 12 }
    default:
      return state
  }
}
