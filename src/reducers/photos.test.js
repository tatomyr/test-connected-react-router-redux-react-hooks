import { types } from "actions/photos"
import { photos as reducer, initialState } from "./photos"

describe("photos reducer", () => {
  it("should return default state", () => {
    expect(reducer()).toEqual(initialState)
  })
  it("should handle INCREASE_COUNT action", () => {
    expect(reducer(undefined, { type: types.INCREASE_COUNT })).toEqual({
      ...initialState,
      count: 23
    })
  })
})
