import { all } from "redux-saga/effects"
import photos from "./photos"
import notiofications from "./notifications"

export function* rootSaga() {
  yield all([photos(), notiofications()])
}
