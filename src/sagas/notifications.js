import { takeEvery, call } from "redux-saga/effects"
import { types as photosTypes } from "actions/photos"
import { errorNotification } from "services/notifications"

export function* errorNotificationSaga(action) {
  yield call(errorNotification(action.message))
}

export default function*() {
  yield takeEvery(photosTypes.GET_LIST.FAILURE, errorNotificationSaga)
}
