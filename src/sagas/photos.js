import { takeEvery, call, put } from "redux-saga/effects"
import { types } from "actions/photos"
import { getList } from "services/photos"

export function* getPhotosSaga() {
  try {
    const list = yield call(getList)
    yield put({ type: types.GET_LIST.SUCCESS, list })
  } catch (err) {
    yield put({ type: types.GET_LIST.FAILURE, ...err })
  }
}

export default function*() {
  yield takeEvery(types.GET_LIST.STARTED, getPhotosSaga)
}
