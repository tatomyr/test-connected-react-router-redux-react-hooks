import { createSelector } from "reselect"
export const selectPhotos = ({ photos }) => photos
export const selectList = state => selectPhotos(state).list
export const selectCount = state => selectPhotos(state).count
export const selectVisibleList = createSelector(
  selectList,
  selectCount,
  (list, count) => list.slice(0, count)
)

export const selectImageInfo = id =>
  createSelector(
    selectList,
    list => list[id] || {}
  )
