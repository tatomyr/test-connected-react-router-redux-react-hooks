export const errorNotification = message => {
  alert(message)
}

export const error = ({ status }) => ({
  status: status,
  message: `Oops. Something went wrong. \nWe've got error with status ${status}.`
})
