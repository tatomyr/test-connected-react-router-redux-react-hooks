import { error } from "./notifications"

const host = "https://picsum.photos"

export const getList = () =>
  fetch(`${host}/list`).then(res => {
    if (res.ok) return res.json()
    throw error(res)
  })

export const imagePath = (id, { width, height }) => {
  if (width && height) {
    const computedWidth = Math.min(width, 1000)
    const computedHeight = (height / width) * computedWidth
    return `${host}/${computedWidth}/${computedHeight}?image=${id}`
  }
  return `${host}/800?image=${id}&blur`
}
